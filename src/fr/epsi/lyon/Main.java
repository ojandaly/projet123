package fr.epsi.lyon;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        ArrayList<Client> clients = new ArrayList<Client>();
        File f = new File("clients.txt");
        ClearClients();
        if (f.exists() && !f.isDirectory()) { //fichier ok
            try (BufferedReader br = new BufferedReader(new FileReader(f))) {
                String line="";
                boolean vide = true;
                while ((line = br.readLine()) != null) {
                    String[] champs = line.split(":");
                    if(champs.length < 4)
                        System.out.println("Il manque des champs dans le fichiers clients !"); //fichier ko
                    else
                    clients.add(new Client(Integer.parseInt(champs[0]), champs[1], champs[2], champs[3]));
                vide = false;
                }
                if(vide)
                    System.out.println("Le fichiers clients est vide !"); //fichier ko

            } catch (Exception e) {
                e.printStackTrace();
            }
            for(Client client : clients)
                InsertClient(client);
        } else
            System.out.println("Le fichier clients n'a pas été trouvé !"); //fichier ko
        new java.util.Scanner(System.in).nextLine();
    }


    private static void InsertClient(Client client) throws ClassNotFoundException, SQLException {
        //fonction d'insertion de client en BDD
        getStatement().execute("INSERT INTO clients (id, nom, prenom, adresse) VALUES (" + client.getId() + ",'" + client.getNom() + "','" + client.getPrenom() + "', '" + client.getAdresse() + "')");
        System.out.println(client.getNom() + " " + client.getPrenom() + " à été inséré dans la base de données.");
    }

    private static void ClearClients() throws SQLException, ClassNotFoundException {
        //fonction de vidage des clients dans la BDD
        getStatement().execute("TRUNCATE clients");
        System.out.println("clients vidés.");
    }

    private static Statement getStatement() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ATK", "root", "");
        return conn.createStatement();
    }
}
